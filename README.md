
# Nooku-Com_emails

## Setup

1.   Set up a symlink `(your website root)/components/com_emails => (code root)/com_emails`
1.   Run the SQL in `sql/db.sql`. Make sure to replace `jos_` with your own database prefix first.
1.   Add a layout to the layouts database table. A layout should contain `{{content}}` once.
1.   Add a template to the templates database table. Here you can use field names like this: `{{name}}`. 
     These act as placeholders for e-mail merges.

## Example usage

    <?php
    
    // Build KCommand Context for email controller
    $data = array(
        'fields' => array(
            'name' => 'jon doe',
            'age'  => '25'
        ),
        'component'  => 'com_foo', // component identifier, references the component column in the templates table
        'type'       => 'bar',     // type identifier, references the type column in templates table
        'recipients' => array('jon@doe.com')
    );

    // Get email controller and send the email
    $this->getService('com://site/emails.controller.email')->send($data);

## Database tables

    jos_emails_templates // these are your content templates, body copy
    jos_emails_layouts   // overall template layout
    jos_emails_emails    // tracking table, contains sent emails
