<?php
/**
 * Com Emails Emails Controller
 *
 * @author      Kyle Waters <kyle@organic-development.com>
 * @author      Mark Head <mark@organic-development.com>
 * @category    Controller
 * @package     Emails
 * @uses        Com_Organic
 */
defined('KOOWA') or die('Restricted Access');

class ComEmailsDatabaseTableEmails extends KDatabaseTableDefault
{
    /**
     * Class Initializer
     *
     * @param KConfig $config
     */
    protected function _initialize( KConfig $config )
    {
        // Set some column filters
        $config->append(array(
                'filters' => array(
                    'html'   => array( 'html', 'tidy' )
                )));

        parent::_initialize( $config );
    }
}
