<?php
/**
 * Com Emails Template Controller
 *
 * @author      Kyle Waters <kyle@organic-development.com>
 * @author      Mark Head <mark@organic-development.com>
 * @category    Controller
 * @package     Emails
 * @uses        Com_Organic
 */
defined( 'KOOWA' ) or die( 'Restricted Access' );

class ComEmailsControllerTemplate extends ComEmailsControllerResource
{
    /**
     * Prevent direct access
     *
     * @param	KCommandContext	A command context object
     * @return 	string|false 	The rendered output of the view or false if something went wrong
     */
    protected function _actionGet(KCommandContext $context)
    {
        $context->setError(new KControllerException(
            JText::_( 'EMAILS_METHOD_NOT_ALLOWED' ), KHttpResponse::METHOD_NOT_ALLOWED
        ));
    }

    /**
     * Render the template
     *
     * @param KCommandContext $context
     * @return mixed
     */
    protected function _actionRender(KCommandContext $context)
	{
		$type = $context->data->get('type','html');
		if(!in_array($type, array('text','html'))) $type = 'html';

		// Pass to the appropriate view
		$this->_view = 'template';
		$this->getRequest()->format = $type;
		$view_html = clone( $this->getView() );

		if($type == 'html'){
			$view_html->css = $context->data->css;
		}

		return $view_html
			->id( $context->data->template_id )
            ->component( $context->data->component )
            ->type( $context->data->type )
			->set( 'fields', $context->data->fields )
			->display();
	}
}
