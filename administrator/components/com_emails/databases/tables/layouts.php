<?php
/**
 * Com Emails Layouts Table
 *
 * @author      Mark Anthony Alcaide <marka@wizmediateam.com>
 * @category    Database Table
 * @package     Emails
 * @uses
 */
defined('KOOWA') or die('Restricted Access');

class ComEmailsDatabaseTableLayouts extends KDatabaseTableDefault
{
    /**
     * Class Initializer
     *
     * @param KConfig $config
     */
    protected function _initialize( KConfig $config )
    {
        // Set some column filters
        $config->append(array(
            'filters' => array(
                'html'   => array( 'html', 'tidy' )
            )));

        parent::_initialize( $config );
    }
}
