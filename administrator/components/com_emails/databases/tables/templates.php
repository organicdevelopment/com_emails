<?php
/**
 * Com Emails Templates Table
 *
 * @author      Mark Anthony Alcaide <marka@wizmediateam.com>
 * @category    Database Table
 * @package     Emails
 * @uses
 */
defined('KOOWA') or die('Restricted Access');

class ComEmailsDatabaseTableTemplates extends KDatabaseTableDefault
{
    /**
     * Class Initializer
     *
     * @param KConfig $config
     */
    protected function _initialize( KConfig $config )
    {
        // Set some column filters
        $config->append(array(
            'filters' => array(
                'body_html'   => array( 'html', 'tidy' )
            )));

        parent::_initialize( $config );
    }
}
