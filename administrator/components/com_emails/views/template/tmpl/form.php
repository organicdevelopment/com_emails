<?php
/**
 * ComEmails
 *
 * @author      Mark Anthony Alcaide <marka@wizmediateam.com>
 * @category    Nooku
 * @package     Socialhub
 * @subpackage  Emails
 * @uses
 */

defined('KOOWA') or die('Restricted access') ?>

<?= @helper('behavior.mootools') ?>
<?= @helper('behavior.keepalive') ?>
<?= @helper('behavior.validator') ?>

<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />

<form action="" method="post" class="-koowa-form" id="meet-form">
    <div class="grid_8">
        <div class="panel title clearfix">
            <h3><?= @text('Email Details') ?></h3>
            <table class="admintable">
                <tr>
                    <td class="key">
                        <label for="name">
                            <?= @text('Name') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="name" value="<?= $template->name ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="layout_id">
                            <?= @text('Layout') ?>:
                        </label>
                    </td>
                    <td>
                        <?= @helper('listbox.layout_id', array(
                            'name' => 'layout_id',
                            'model' => 'layouts',
                            'selected' => $template->layout_id,
                            'text' => 'name',
                            'value' => 'id',
                        )) ?>
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="component">
                            <?= @text('Component') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="component" value="<?= $template->component ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="type">
                            <?= @text('Type') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="type" value="<?= $template->type ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="subject">
                            <?= @text('Subject') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="subject" value="<?= $template->subject ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="from_name">
                            <?= @text('From Name') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="from_name" value="<?= $template->from_name ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="from_email">
                            <?= @text('From Email') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="from_email" value="<?= $template->from_email ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="body_text">
                            <?= @text('Body (Text)') ?>:
                        </label>
                    </td>
                    <td>
                        <textarea id="body_text" name="body_text" rows="6" cols="60"><?= $template->body_text ?></textarea>
                    </td>
                </tr>
                 <tr>
                    <td class="key">
                        <label for="body_html">
                            <?= @text('Body (HTML)') ?>:
                        </label>
                    </td>
                    <td>
                        <?= @editor(array(
                            'name' => 'body_html',
                            'html' => $template->body_html,
                            'width' => '100%',
                            'height' => '200',
                            'cols' => '60',
                            'rows' => '20',
                            'buttons' => true,
                            'options' => array('theme' => 'simple', 'pagebreak', 'readmore')
                        )) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>