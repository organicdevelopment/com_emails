<?php
/**
 * ComEmails
 *
 * @author      Mark Anthony Alcaide <marka@wizmmediateam.com>
 * @category    Nooku
 * @package     Socialhub
 * @subpackage  ComEmails
 * @uses
 */

defined('KOOWA') or die('Restricted Access') ?>

<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />

<form action="<?= @route(); ?>" method="get" class="-koowa-grid">
    <table class="adminlist">
        <thead>
            <tr>
                <th width="10">
                    <?= @helper('grid.checkall') ?>
                </th>
                <th>
                    <?= @helper('grid.sort', array('column' => 'name', 'title' => 'Name')) ?>
                </th>
            </tr>
        </thead>

        <tfoot>
               <tr>
                    <td colspan="13">
                         <?= @helper('paginator.pagination', array('total' => $total)) ?>
                    </td>
                </tr>
        </tfoot>

        <tbody>
        <?php foreach ($layouts as $layout) : ?>
            <tr>
                <td align="center">
                    <?= @helper('grid.checkbox' , array('row' => $layout)) ?>
                </td>

                <td align="left">
                    <span class="editlinktip hasTip" title="<?= @text('Edit Template') ?>:: <?= @escape($layout->name) ?>">
                        <a href="<?= @route('view=layout&id='.$layout->id) ?>">
                            <?= $layout->name ?>
                        </a>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php if (!count($layouts)) : ?>
            <tr>
                <td colspan="20" align="center">
                    <?= @text('No Items Found') ?>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>