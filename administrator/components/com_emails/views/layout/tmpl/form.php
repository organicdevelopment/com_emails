<?php
/**
 * ComEmails
 *
 * @author      Mark Anthony Alcaide <marka@wizmediateam.com>
 * @category    Nooku
 * @package     Socialhub
 * @subpackage  Emails
 * @uses
 */

defined('KOOWA') or die('Restricted access') ?>

<?= @helper('behavior.mootools') ?>
<?= @helper('behavior.keepalive') ?>
<?= @helper('behavior.validator') ?>

<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />

<form action="" method="post" class="-koowa-form" id="meet-form">
    <div class="grid_8">
        <div class="panel title clearfix">
            <h3><?= @text('Layout Details') ?></h3>
            <table class="admintable">
                <tr>
                    <td class="key">
                        <label for="name">
                            <?= @text('Name') ?>:
                        </label>
                    </td>
                    <td>
                        <input class="required" type="text" name="name" value="<?= $layout->name ?>" size="80" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="text">
                            <?= @text('Text') ?>:
                        </label>
                    </td>
                    <td>
                        <textarea id="text" name="text" rows="6" cols="60"><?= $layout->text ?></textarea>
                    </td>
                </tr>
                 <tr>
                    <td class="key">
                        <label for="html">
                            <?= @text('HTML') ?>:
                        </label>
                    </td>
                    <td>
                        <?= @editor(array(
                            'name' => 'html',
                            'html' => $layout->html,
                            'width' => '100%',
                            'height' => '200',
                            'cols' => '60',
                            'rows' => '20',
                            'buttons' => true,
                            'options' => array('theme' => 'simple', 'pagebreak', 'readmore')
                        )) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>