<?php
/**
 * ComEmails
 *
 * @author      Mark Anthony Alcaide <marka@wizmmediateam.com>
 * @category    Nooku
 * @package     Socialhub
 * @subpackage  ComEmails
 * @uses
 */

defined('KOOWA') or die('Restricted Access') ?>

<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />

<form action="<?= @route(); ?>" method="get" class="-koowa-grid">
    <table class="adminlist">
        <thead>
            <tr>
                <th width="10">
                    <?= @helper('grid.checkall') ?>
                </th>
                <th>
                    <?= @helper('grid.sort', array('column' => 'name', 'title' => 'Name')) ?>
                </th>
                <th width="12%">
                    <?= @helper('grid.sort', array('column' => 'subject', 'state' => $state, 'title' => 'Subject')) ?>
                </th>
                <th width="15%">
                    <?= @helper('grid.sort', array('column' => 'from_name', 'state' => $state, 'title' => 'From Name')) ?>
                </th>
                <th width="15%">
                    <?= @helper('grid.sort', array('column' => 'from_email', 'state' => $state, 'title' => 'From Email')) ?>
                </th>
            </tr>
        </thead>

        <tfoot>
               <tr>
                    <td colspan="13">
                         <?= @helper('paginator.pagination', array('total' => $total)) ?>
                    </td>
                </tr>
        </tfoot>

        <tbody>
        <?php foreach ($templates as $template) : ?>
            <tr>
                <td align="center">
                    <?= @helper('grid.checkbox' , array('row' => $template)) ?>
                </td>

                <td align="left">
                    <span class="editlinktip hasTip" title="<?= @text('Edit Template') ?>:: <?= @escape($template->name) ?>">
                        <a href="<?= @route('view=template&id='.$template->id) ?>">
                            <?= $template->name ?>
                        </a>
                    </span>
                </td>
                <td align="left">
                    <?= @escape($template->subject) ?>
                </td>
                <td align="left">
                    <?= @escape($template->from_name) ?>
                </td>
                <td align="left">
                    <?= @escape($template->from_email) ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php if (!count($templates)) : ?>
            <tr>
                <td colspan="20" align="center">
                    <?= @text('No Items Found') ?>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>